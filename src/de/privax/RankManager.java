package de.privax;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.privax.units.MySQL;

public class RankManager {

	public static boolean UserExists(String UUID){
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT UUID FROM ranks WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs =ps.executeQuery();
				return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean timeout(String UUID){
		long current = System.currentTimeMillis();
		long end = 0;
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT end FROM ranks WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				end = rs.getLong("end");
			}
		} catch (SQLException e) {
			
		}
		if(end == -1){
			return false;
		} else {
			if(current > end){
				setrank(UUID, "Spieler", -1);
				return true;
			} else {
				return false;
			}
		}
				
	}
	
	public static void setrank(String UUID, String rank, long time){
		if(time == -1){
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE ranks SET rank=?,end=-1 WHERE UUID = ?");
				ps.setString(1, rank);
				ps.setString(2, UUID);
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE ranks SET rank=?,end="+time+" WHERE UUID = ?");
				ps.setString(1, rank);
				ps.setString(2, UUID);
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
