package de.privax.units;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import de.privax.Main;

public class MySQL {

	public static Connection con;
	
	public static void connect(){
		if(!isconected()){
			try {
				System.out.println(Main.port);
				con = DriverManager.getConnection("jdbc:mysql://"+Main.host+":"+Main.port+"/"+Main.database+"?autoReconnect=true", Main.username, Main.password);
			} catch (SQLException e) {
				e.printStackTrace();
				
			}
		}
	}
	
	public static void disconnect(){
		if(isconected()){
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static boolean isconected(){
		return (con == null ? false : true);
		
	}
	public static Connection getConnection(){
		return con;
	}
}
