package de.privax;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.privax.cmd.ban;
import de.privax.cmd.mute;
import de.privax.cmd.ping;
import de.privax.cmd.rank;
import de.privax.cmd.report;
import de.privax.cmd.unban;
import de.privax.cmd.unmute;
import de.privax.events.Chat;
import de.privax.events.Login;
import de.privax.events.ServerConnect;
import de.privax.events.TabComplete;
import de.privax.units.MySQL;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Main extends Plugin {
	
	public static String prefix = "�8[�cThePorx�8]";
	
	public static String username;
	public static String password;
	public static String host;
	public static String port;
	public static String database;

	public static ArrayList<ProxiedPlayer> muted = new ArrayList<>();
	
	public void onEnable() {
		BungeeCord.getInstance().getScheduler().schedule(this, new Runnable() {
			
			@Override
			public void run() {
				Chat.chatabuse.clear();
			}
		}, 10, 10, TimeUnit.MINUTES);
	    BungeeCord.getInstance().getConsole().sendMessage(new TextComponent(prefix + " �aAktive"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new ping("ping"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new ban("ban"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new unban("unban"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new mute("mute"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new unmute("unmute"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new report("report"));
	    BungeeCord.getInstance().getPluginManager().registerCommand(this, new rank("rank", this));
	    BungeeCord.getInstance().getPluginManager().registerListener(this, new Login());
	    BungeeCord.getInstance().getPluginManager().registerListener(this, new TabComplete());
	    BungeeCord.getInstance().getPluginManager().registerListener(this, new Chat());
	    BungeeCord.getInstance().getPluginManager().registerListener(this, new ServerConnect());
	    try{
		    if(!getDataFolder().exists()){
		    	getDataFolder().mkdirs();
		    }
		    File config = new File(getDataFolder().getPath(), "config.yml");
		    File permissions = new File(getDataFolder().getPath(), "permissions.yml");
		    if(!config.exists()){
		    	config.createNewFile();
		    	Configuration configs = ConfigurationProvider.getProvider(YamlConfiguration.class).load(config);
		    	configs.set("mysql.username", "user");
		    	configs.set("mysql.password", "password");
		    	configs.set("mysql.port", "port");
		    	configs.set("mysql.host", "host");
		    	configs.set("mysql.database", "database");
		    	ConfigurationProvider.getProvider(YamlConfiguration.class).save(configs, config);
		    }
		    if(!permissions.exists()){
		    	permissions.createNewFile();
		    	Configuration permmission = ConfigurationProvider.getProvider(YamlConfiguration.class).load(permissions);
		    	List<String> groups = new ArrayList<>();
		    	groups.add("Spieler");
		    	groups.add("Admin");
		    	permmission.set("groups", groups);
		    	List<String> rechte = new ArrayList<>();
		    	rechte.add("rank.group");
		    	rechte.add("rank.manager");
		    	rechte.add("player.admin");
		    	permmission.set("permissions.Admin", rechte);
		    	ConfigurationProvider.getProvider(YamlConfiguration.class).save(permmission, permissions);
		    }
	    	Configuration configs = ConfigurationProvider.getProvider(YamlConfiguration.class).load(config);
	    	username = configs.getString("mysql.username");
	    	password = configs.getString("mysql.password");
	    	host = configs.getString("mysql.host");
	    	port = configs.getString("mysql.port");
	    	database = configs.getString("mysql.database");
	    	MySQL.connect();
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS bans(UUID VARCHAR(100), Grund VARCHAR(100), end BIGINT(255))");
			    ps.executeUpdate();
				PreparedStatement ps2 = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS banhistory(UUID VARCHAR(100), bans INT(100))");
			    ps2.executeUpdate();
				PreparedStatement ps3 = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS mute(UUID VARCHAR(100), Grund VARCHAR(100), end BIGINT(255))");
			    ps3.executeUpdate();
				PreparedStatement ps4 = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS mutehistory(UUID VARCHAR(100), mutes INT(100))");
			    ps4.executeUpdate();
				PreparedStatement ps5 = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS ranks(UUID VARCHAR(100), rank VARCHAR(100), end BIGINT(255))");
			    ps5.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		    
		    
	    } catch (IOException e) {
			e.printStackTrace();
		}
	}
}