package de.privax;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.privax.units.MySQL;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MuteManager {

	
	public static void Mute(String playername, long end, String grund, String timeformessage){
		if(UUIDFlecther.getUUID(playername) == null){
			return;
		}
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO mute(UUID, Grund, end) VALUES (?,?,?)");
			ps.setString(1, UUIDFlecther.getUUID(playername));
			ps.setString(2, grund);
			ps.setLong(3, end);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		BungeeCord.getInstance().getConsole().sendMessage(new TextComponent("�8[�cMute�8] �e"+playername+"�c Grund�8:�e"+grund+"�c Zeit�8: �e"+timeformessage));
		for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
			if(all.hasPermission("ban")){
				all.sendMessage(new TextComponent("�8[�cMute�8] �e"+playername+"�c Grund�8:�e"+grund+"�c Zeit�8: �e"+timeformessage));
			}
		}
	}
	
	public static String getTime(String UUID){
		long current = System.currentTimeMillis();
		long end = 0;
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT end FROM mute WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				end = rs.getLong("end");
			}
		} catch (SQLException e) {
			
		}
		long time = end-current;
		if(end == -1){
			return "�4Permanent";
		} else {
				int sec = 0;
				int min = 0;
				int h = 0;
				int d = 0;
				while (time > 1000) {
					time-=1000;
					sec++;
				}
				while (sec > 60) {
					sec-=60;
					min++;
				}
				while (min > 60) {
					min-=60;
					h++;
				}
				while (h > 24) {
					h-=24;
					d++;
				}
				return "�e"+d+" �cTag(e) �e"+h+" �cStunde(n) �e"+min+" �cMinute(n) �e"+sec+" �cSekunde(n)";
		}
				
	}
	
	public static String getReason(String UUID){
		try{
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT Grund FROM mute WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString("Grund");
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return "�cFehler";
	}
	
	
	public static boolean usermutehistory(String UUID){
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT mutes FROM mutehistory WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	public static void addmute(String UUID){
		if(usermutehistory(UUID)){
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE mutehistory SET mutes="+(getmutes(UUID)+1)+" WHERE UUID = ?");
				ps.setString(1, UUID);
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			
	}

	
	public static Integer getmutes(String UUID){
		if(usermutehistory(UUID)){
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT mutes FROM mutehistory WHERE UUID = ?");
				ps.setString(1, UUID);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					return rs.getInt("mutes");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return 0;
		} else {
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO mutehistory(UUID, mutes) VALUES (?,?)");
				ps.setInt(2, 0);
				ps.setString(1, UUID);
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return 0;
		}
	}
	
	public static boolean ismuted(String UUID){
		long current = System.currentTimeMillis();
		long end = 0;
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT end FROM mute WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				end = rs.getLong("end");
			}
		} catch (SQLException e) {
			
		}
		if(end == -1){
			return true;
		} else if(current > end){
				try {
					PreparedStatement ps = MySQL.getConnection().prepareStatement("DELETE FROM mute WHERE UUID = ?");
					ps.setString(1, UUID);
					ps.executeUpdate();
				} catch (SQLException e) {
					
				}
				return false;
		} else {
			return true;
		}
	}
	
}
