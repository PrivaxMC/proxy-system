package de.privax.events;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerConnect implements Listener {

	@EventHandler
	public void onServerConnect(ServerConnectedEvent e){
		ProxiedPlayer p = e.getPlayer();
		if(p.hasPermission("player.admin")){
			p.setDisplayName("§4Admin §7● "+p.getName());
		} else if(p.hasPermission("player.dev")){
			p.setDisplayName("§cDev §7●"+p.getName());
		} else if(p.hasPermission("player.srmod")){
			p.setDisplayName("§cSrMod §7● "+p.getName());
		} else if(p.hasPermission("player.mod")){
			p.setDisplayName("§cMod §7● "+p.getName());
		} else if(p.hasPermission("player.sup")){
			p.setDisplayName("§cSup §7● "+p.getName());
		} else if(p.hasPermission("player.builder")){
			p.setDisplayName("§eBuilder §7●"+p.getName());
		} else if(p.hasPermission("player.youtuber")){
			p.setDisplayName("§5YouTuber §7● "+p.getName());
		} else if(p.hasPermission("player.emerald")){
			p.setDisplayName("§aEmerald §7● "+p.getName());
		} else if(p.hasPermission("player.diamond")){
			p.setDisplayName("§bDiamond §7● "+p.getName());
		} else if(p.hasPermission("player.gold")){
			p.setDisplayName("§6Gold §7● "+p.getName());
		} else {
			p.setDisplayName("§9Spieler §7● "+p.getName());
		}
		
	}
	
}
