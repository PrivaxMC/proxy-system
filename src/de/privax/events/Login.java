package de.privax.events;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import de.privax.BanManager;
import de.privax.RankManager;
import de.privax.units.MySQL;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Login implements Listener {

	@EventHandler
	public void onlogin(PreLoginEvent e){
		String UUID = UUIDFlecther.getUUID(e.getConnection().getName());
		if(!RankManager.UserExists(UUID)){
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO ranks(UUID, rank, end) VALUES (?,?,?)");
				ps.setString(1, UUID);
				ps.setString(2, "Spieler");
				ps.setInt(3, -1);
				ps.executeUpdate();
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
	}
	
	@EventHandler
	public void login(PreLoginEvent e){
		String name = e.getConnection().getName();
		if(BanManager.isbanned(UUIDFlecther.getUUID(name))){
			e.setCancelled(true);
			e.setCancelReason(new TextComponent("�cDu wurdest gebannt"
					+ "\n"
					+ "\n"
					+ "�cGrund�8:�e"+BanManager.getReason(UUIDFlecther.getUUID(name))
					+ "\n"
					+ "�cVerbleibende Zeit�8: �e"+BanManager.getTime(UUIDFlecther.getUUID(name))));
		}
	}
	
}
