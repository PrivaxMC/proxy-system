package de.privax.events;

import java.util.ArrayList;
import java.util.HashMap;

import de.privax.MuteManager;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Chat implements Listener{
	
	public static HashMap<ProxiedPlayer, Integer> chatabuse = new HashMap<>();
	
	@EventHandler
	public static void chatmutemanager(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if(!e.getMessage().contains("/")){
			if(MuteManager.ismuted(UUIDFlecther.getUUID(p.getName()))){
				e.setCancelled(true);
				p.sendMessage(new TextComponent("§8[§cMute§8] §cDu bist noch §e"+MuteManager.getTime(UUIDFlecther.getUUID(p.getName()))+" gemuted §cGrund§8:§e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
			} else {
				ArrayList<String> blacklist = new ArrayList<>();
				blacklist.add("fuck");
				blacklist.add("fick");
				blacklist.add("arschloch");
				blacklist.add("jude");
				blacklist.add("hitler");
				blacklist.add("gaskammer");
				blacklist.add("卐");
				blacklist.add("卍");
				blacklist.add("nigger");
				blacklist.add("hure");
				blacklist.add("hurensohn");
				blacklist.add("nutte");
				for (int i = 0; i < blacklist.size(); i++) {
					if(e.getMessage().toLowerCase().contains(blacklist.get(i))){
						e.setCancelled(true);
						p.sendMessage(new TextComponent("§cBitte achte auf deine Wortwahl."));
						if(!chatabuse.containsKey(p)){
							chatabuse.put(p, 1);
						} else {
							chatabuse.replace(p, chatabuse.get(p).intValue()+1);
						}
						if(chatabuse.get(p) >= 4){
							String msg =  " Beleidunung";
							String UUID = UUIDFlecther.getUUID(p.getName());
							int mutenow = MuteManager.getmutes(UUIDFlecther.getUUID(p.getName()));
							if(mutenow >= 10){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), -1, msg, "§4Permanent");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu §4Permanent §c aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 9){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*60)+System.currentTimeMillis(), msg, "60 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e60§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 8){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*30)+System.currentTimeMillis(), msg, "30 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e30§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 7){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*10)+System.currentTimeMillis(), msg, "10 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e10§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 6){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*2)+System.currentTimeMillis(), msg, "2 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e2 §cTag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 5){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24)+System.currentTimeMillis(), msg, "1 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e1 §cTag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 4){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*12)+System.currentTimeMillis(), msg, "12 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e12§c §cStunde(n) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 3){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*8)+System.currentTimeMillis(), msg, "8 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e8§c Stunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 2){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*4)+System.currentTimeMillis(), msg, "4 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e4§c Stunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else {
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*1)+System.currentTimeMillis(), msg, "1 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e1 §cStunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							}
						}
						return;
					}
				}
			}
		}
	}	@EventHandler
	public static void cahtwerbung(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if(!e.getMessage().contains("/")){
			if(MuteManager.ismuted(UUIDFlecther.getUUID(p.getName()))){
				e.setCancelled(true);
			} else {
				ArrayList<String> blacklist = new ArrayList<>();
				blacklist.add(".de");
				blacklist.add(".net");
				blacklist.add(".com");
				blacklist.add(".at");
				blacklist.add(".biz");
				blacklist.add(".info");
				blacklist.add(".tv");
				blacklist.add(".org");
				blacklist.add(".eu");
				blacklist.add(".server");
				blacklist.add(".name");
				blacklist.add(".me");
				blacklist.add(".d e");
				blacklist.add(".n e t");
				blacklist.add(".n et");
				blacklist.add(".ne t");
				blacklist.add(".c o m");
				blacklist.add(".co m");
				blacklist.add(".c om");
				blacklist.add(".a t");
				blacklist.add(".b i z");
				blacklist.add(".b iz");
				blacklist.add(".bi z");
				blacklist.add(".i n f o");
				blacklist.add(".s e r v e r");
				blacklist.add(".n a m e");
				blacklist.add(".m e");
				for (int i = 0; i < blacklist.size(); i++) {
					if(e.getMessage().toLowerCase().contains(blacklist.get(i))){
						e.setCancelled(true);
						p.sendMessage(new TextComponent("§cBitte mach keine Werbung."));
						if(!chatabuse.containsKey(p)){
							chatabuse.put(p, 1);
						} else {
							chatabuse.replace(p, chatabuse.get(p).intValue()+1);
						}
						if(chatabuse.get(p) >= 4){
							String msg =  " Werbung";
							String UUID = UUIDFlecther.getUUID(p.getName());
							int mutenow = MuteManager.getmutes(UUIDFlecther.getUUID(p.getName()));
							if(mutenow >= 10){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), -1, msg, "§4Permanent");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu §4Permanent §c aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 9){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*60)+System.currentTimeMillis(), msg, "60 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e60§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 8){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*30)+System.currentTimeMillis(), msg, "30 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e30§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 7){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*10)+System.currentTimeMillis(), msg, "10 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e10§c Tag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 6){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24*2)+System.currentTimeMillis(), msg, "2 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e2 §cTag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 5){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*24)+System.currentTimeMillis(), msg, "1 §cTag(e)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e1 §cTag(e) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 4){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*12)+System.currentTimeMillis(), msg, "12 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e12§c §cStunde(n) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 3){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*8)+System.currentTimeMillis(), msg, "8 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e8§c Stunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else if(mutenow == 2){
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*4)+System.currentTimeMillis(), msg, "4 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e4§c Stunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							} else {
								MuteManager.addmute(UUID);
								MuteManager.Mute(p.getName(), ((long)1000*60*60*1)+System.currentTimeMillis(), msg, "1 §cStunde(n)");
								p.sendMessage(new TextComponent("§8[§cMute§8] §cDu wurdest für §e1 §cStunde(m) aus dem chat gebannt Grund§8:§e"+MuteManager.getReason(UUID)));
							}
						}
						return;
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onChat(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if(e.getMessage().toLowerCase().startsWith("@t")){
			String msg = e.getMessage().replace("@t", "").replace("@T", "");
			if(e.getSender() instanceof ProxiedPlayer){
				if(p.hasPermission("teamchat")){
					for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
						if(all.hasPermission("teamchat")){
							all.sendMessage(new TextComponent("§8[§cTeam-Chat§8] §f"+p.getDisplayName()+" §8»§e"+msg));
							e.setCancelled(true);
						}
					}
				}
			}
		}
	}
}
