package de.privax.events;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class TabComplete implements Listener {

	@EventHandler
	public void onTab(TabCompleteEvent e){
		if(e.getCursor().toLowerCase().startsWith("/ban ") | e.getCursor().startsWith("/mute ") | e.getCursor().startsWith("/unmute ") | e.getCursor().startsWith("/report ")){
			for(ProxiedPlayer p : BungeeCord.getInstance().getPlayers()){
				e.getSuggestions().add(p.getName());
			}
		}
	}
	
}
