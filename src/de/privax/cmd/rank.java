package de.privax.cmd;

import java.io.File;
import java.io.IOException;
import java.util.List;

import de.privax.Main;
import de.privax.RankManager;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class rank extends Command {

	Main plugin;
	
	public rank(String name, Main plugin) {
		super(name);
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("rank.group")){
			if(args.length == 4){
				if(args[0].equalsIgnoreCase("set")){
					if(UUIDFlecther.getUUID(args[1]) != null){
						try {
							File permissions = new File(plugin.getDataFolder().getPath(), "permissions.yml");
							Configuration permission = ConfigurationProvider.getProvider(YamlConfiguration.class).load(permissions);
					    	List<String> groups = permission.getStringList("groups");
					    	if(groups.contains(args[2])){
					    		if(args[3].equalsIgnoreCase("lifetime")){
					    			RankManager.setrank(UUIDFlecther.getUUID(args[1]), args[2], -1);
					    		} else if(Integer.valueOf(args[3]) != null){
					    			RankManager.setrank(UUIDFlecther.getUUID(args[1]), args[2], 1000*60*60*24*(long)Long.valueOf(args[3])+System.currentTimeMillis());
					    		}
					    	}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} if (sender.hasPermission("rank.manage")){
			
		}
	}

}
