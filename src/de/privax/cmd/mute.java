package de.privax.cmd;

import de.privax.MuteManager;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class mute extends Command{

	public mute(String name) {
		super(name);
	}

	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("ban")){
			if(args.length > 1){
				if(UUIDFlecther.getUUID(args[0]) == null){
					sender.sendMessage(new TextComponent("�8[�cMute�8] �cDiesen Spieler giebt es nicht"));
				} else {
					String UUID = UUIDFlecther.getUUID(args[0]);
					int mutenow = MuteManager.getmutes(UUID);
					String msg = "";
					for (int i = 1; i < args.length; i++) {
						msg = msg + " "+args[i];
						
					}
					ProxiedPlayer p = BungeeCord.getInstance().getPlayer(args[0]);
					if(mutenow >= 10){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], -1, msg, "�4Permanent");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu �4Permanent �c aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 9){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*24*60)+System.currentTimeMillis(), msg, "60 �cTag(e)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 60 Tag(e) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 8){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*24*30)+System.currentTimeMillis(), msg, "30 �cTag(e)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 30 Tag(e) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 7){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*24*10)+System.currentTimeMillis(), msg, "10 �cTag(e)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 10 Tag(e) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 6){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*24*2)+System.currentTimeMillis(), msg, "2 �cTag(e)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 2 �cTag(e) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 5){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*24)+System.currentTimeMillis(), msg, "1 �cTag(e)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
						
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 1 �cTag(e) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 4){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*12)+System.currentTimeMillis(), msg, "12 �cStunde(n)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 12 �cStunde(n) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 3){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*8)+System.currentTimeMillis(), msg, "8 �cStunde(n)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 8 Stunde(m) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else if(mutenow == 2){
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*4)+System.currentTimeMillis(), msg, "4 �cStunde(n)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 4 Stunde(m) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					} else {
						MuteManager.addmute(UUID);
						MuteManager.Mute(args[0], ((long)1000*60*60*1)+System.currentTimeMillis(), msg, "1 �cStunde(n)");
						if(BungeeCord.getInstance().getPlayers().contains(p)){
							p.sendMessage(new TextComponent("�8[�cMute�8] �cDu wurdest f�r 1 Stunde(m) aus dem chat gebannt Grund�8:�e"+MuteManager.getReason(UUIDFlecther.getUUID(p.getName()))));
						}
					}
				}
			}
		} else {
			sender.sendMessage(new TextComponent("�8[�cBan�8] �cDazu hast du keine Rechte!"));
		}
	}

}
