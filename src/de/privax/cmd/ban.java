package de.privax.cmd;

import de.privax.BanManager;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ban extends Command{

	public ban(String name) {
		super(name);
	}

	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("ban")){
			if(args.length > 1){
				if(UUIDFlecther.getUUID(args[0]) == null){
					sender.sendMessage(new TextComponent("�8[�cBan�8] �cDiesen Spieler giebt es nicht"));
				} else {
					String UUID = UUIDFlecther.getUUID(args[0]);
					int bansnow = BanManager.getbans(UUID);
					String msg = "";
					for (int i = 1; i < args.length; i++) {
						msg = msg + " "+args[i];
						
					}
					ProxiedPlayer p = BungeeCord.getInstance().getPlayer(args[0]);
					if(bansnow >= 2){
						BanManager.addbans(UUID);
						BanManager.ban(args[0], -1, msg, "�4Permanent");
					} else if(bansnow == 1){
						BanManager.addbans(UUID);
						BanManager.ban(args[0], 7776000000L+System.currentTimeMillis(), msg, "90 �cTag(e)");
					} else {
						BanManager.addbans(UUID);
						BanManager.ban(args[0], 2592000000L+System.currentTimeMillis(), msg, "30 �cTag(e)");
					}
					if(BungeeCord.getInstance().getPlayers().contains(p)){
						p.disconnect(new TextComponent("�cDu wurdest gebannt"
							+ "\n"
							+ "\n"
							+ "�cGrund�8:�e"+BanManager.getReason(UUID)
							+ "\n"
							+ "�cVerbleibende Zeit�8: �e"+BanManager.getTime(UUID)));
					}
				}
			}
		} else {
			sender.sendMessage(new TextComponent("�8[�cBan�8] �cDazu hast du keine Rechte!"));
		}
	}

}
