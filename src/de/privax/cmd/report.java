package de.privax.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class report extends Command {

	private String prefix = "�8[�c�lReport�8]";
	
	public report(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if(args.length > 0){
			if(BungeeCord.getInstance().getPlayer(args[0]) != null){
				p.sendMessage(new TextComponent(prefix+" �7Danke f�r deinen Report."));
				ProxiedPlayer reportuser = BungeeCord.getInstance().getPlayer(args[0]);
				if(BungeeCord.getInstance().getPlayers().contains(reportuser)){
					for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
						if(all.hasPermission("report")){
							all.sendMessage(new TextComponent(prefix+" �e"+reportuser.getName()+"�c wurde Reportet"));
							TextComponent tp = new TextComponent("[TELEPORTIEREN]");
							tp.setColor(ChatColor.GREEN);
							tp.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/server "+reportuser.getServer().getInfo().getName()));
							all.sendMessage(tp);
						}
					}
				}
			}
		} else {
			p.sendMessage(new TextComponent(prefix+"�e /report �8[�7Name�8]"));
		}
	}

}
