package de.privax.cmd;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import de.privax.BanManager;
import de.privax.units.MySQL;
import de.privax.units.UUIDFlecther;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class unban extends Command{

	public unban(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length > 1){
			if(sender.hasPermission("ban")){
				if(UUIDFlecther.getUUID(args[0]) != null){
					String msg = "";
					for(int i = 1; i < args.length; i++){
						msg = msg+" �e"+args[i];
					}
					if(BanManager.isbanned(UUIDFlecther.getUUID(args[0]))){
						try {
							PreparedStatement ps = MySQL.getConnection().prepareStatement("DELETE FROM bans WHERE UUID = ?");
							ps.setString(1, UUIDFlecther.getUUID(args[0]));
							ps.executeUpdate();
						} catch (SQLException e) {
							
						}
						BungeeCord.getInstance().getConsole().sendMessage(new TextComponent("�8[�cBan�8] �e"+args[0]+" �cWurde entbannt Grund�8:�e"+msg));
						for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
							if(all.hasPermission("ban")){
								all.sendMessage(new TextComponent("�8[�cBan�8] �e"+args[0]+" �cWurde entbannt Grund�8:�e"+msg));
							}
						}
					}
				}
			}
		}
	}

}
